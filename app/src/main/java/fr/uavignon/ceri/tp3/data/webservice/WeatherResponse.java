package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {

    public final Main main = null;

    public final Wind wind = null;

    public final Clouds clouds = null;

    public final Integer dt = null;

    public final List<Weather> weather = null;

    public static class Weather {
        public final String description = null;
        public final String icon = null;
    }

    public static class Main{
        public final Float temp = null;
        public final Integer humidity = null;
    }

    public static class Clouds{
        public final Integer all = null;
    }

    public static class Wind{
        public final Float speed = null;
        public final Integer deg = null;
    }
}
